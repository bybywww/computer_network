# [参考项目](https://github.com/hua1995116/webchat)



# 提示

- 上传图片前端路径: `备用方案\webchat-master\client\src\view\Chat.vue`的66行左右

```html
<div class="fun-li" @click="imgupload">
            <i class="icon iconfont icon-camera"></i>
</div>
```

- 上传图片后端: `webchat\router\files.js`
- 上传文件的技术: 二选一
  - **websocket**  
    - 读取文件到内存(小文件)
    - 用websocket上传  `socket.emit('message'...)`
  - http: 跟上传图片较为类似
    - 客户端文件上传-->服务端
    - 服务端返回文件保存地址-->客户端
    - 客户端发送改链接至其他人
  - 上传文件的图标 
- 语音聊天(如果用websocket的话): `webchat\server_modules\websocket.js`
  - https://www.cnblogs.com/shihuc/p/7603600.html
  - https://github.com/lq782655835/live-video-demo 这个是直播的
- 语言和视频聊天(webRTC) 
  - https://zhuanlan.zhihu.com/p/59520779



# 文件目录

- webchat下
  - **client: 客户端即网页**
    - config: 配置
    - node_modules:  安装的各种依赖项
    - public: 项目的一个图标
    - **src: 源代码**
      - api: 接口
      - assets: 图片
      - component: 自动生成的
      - const: 常数
      - directive: 图片尺寸
      - mixin: 登录
      - router: 路由
      - store:  还是一些常数
      - styles: css,控制格式的
      - utils: 辅助的
      - **view: 主要的视图, 类似于html**
        - **主要是Chat.vue** 
          - 换皮的html区
          - 换皮的js区
        - 其他的就不管了
      - 剩下的就不管了
    - 其他的基本是自动生成的
  - config: 配置
  - deploy: 配置+1
  - dist: 图片
  - models: 可看成自动生成的模块
  - node_modules: 安装的各种依赖项
  - router:  html服务器
  - schemas: 数据库交互
  - **server_modules: 服务器文件**
    - websocket.js文件中`socket.on('message'....)`
  - shark_pic: 可看成自动生成的配置
  - static_temp: 临时静态文件
  - utils: 可看成自动生成文件
  - 剩下的文件基本都是自动生成的



## 功能

- [x] 注册+登录

- [x] 群聊

- [x] 加好友私聊

- [x] 发图片+表情😁

- [x] 改头像

- [x] 搜索好友

- [x] 查看好友资料

- [x] 热门好友推荐

- [x] 显示聊天室的人数

- [x] 简易视频聊天

  





# 如何运行?



## 本地环境设置

- MongoDB
  - [安装地址](https://www.mongodb.com/download-center/community)
  - 提示: 不要选 `install MonogDB compass`
  - [安装教程](https://www.runoob.com/mongodb/mongodb-window-install.html)
    - 注意安装教程中, **命令行下运行 MongoDB 服务器** 和 **配置 MongoDB 服务** 任选一个方式启动就可以。
  - 记得加上环境变量
- Node 8.5.0+ 和 Npm 5.3.0+
  - [安装教程](https://www.runoob.com/nodejs/nodejs-install-setup.html)
  - **配置npm下载镜像源**  
    - 运行以下命令 假设大家都在win10下开发
    - `npm config set registry https://registry.npm.taobao.org`
    - `npm config get registry`
      - ![image-20200626102819869](C:\Users\13298\Pictures\typora\image-20200626102819869.png)





# 安装中可能遇到的问题







- `webchat/client`目录下运行`npm run serve`

  

  > 问题:
  >
  > This dependency was not found:
  >
  > * axios in ./src/api/axios.js

  解决方案:

  `npm install --save axios`

- `webchat`下  `npm run dev`

  >'nodemon' 不是内部或外部命令，也不是可运行的程序
  >或批处理文件。

  解决方案: 安装 `nodemon`

  `npm install -g nodemon`  (这个是全局安装)